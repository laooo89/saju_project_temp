package com.hoonb.sajutemp.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.hoonb.sajutemp.R;

import java.util.ArrayList;

public class TempDataUtil {

    private static final int NUMBER_ASSET_IMAGE = 11;
    private static int sBitmapCounter  = 0;
    private static Bitmap mBitmap1;
    private static Bitmap mBitmap2;
    private static Bitmap mBitmap3;
    private static Bitmap mBitmap4;
    private static Bitmap mBitmap5;
    private static Bitmap mBitmap6;
    private static Bitmap mBitmap7;
    private static Bitmap mBitmap8;
    private static Bitmap mBitmap9;
    private static Bitmap mBitmap10;
    private static Bitmap mBitmap11;
    public static ArrayList<HomeCardData> getDummyHomeItem() {
        ArrayList<HomeCardData> data = new ArrayList<>();

        HomeCardData temp = new HomeCardData(System.currentTimeMillis(), "2020년 이직할 수 있는지 걱정입니다.",
                new ArrayList<String>());
        temp.replyId  =  "도담선생";
        temp.replyDate = System.currentTimeMillis();
        temp.likeCount = 6;
        temp.tags.add("이직");
        temp.tags.add("직장");
        ArrayList<SimpleComment> comments = new ArrayList<>();
        comments.add(new SimpleComment("tte", "좋네요."));
        comments.add(new SimpleComment("tte", "좋네요."));
        temp.commentList = comments;
        data.add(temp);

        temp = new HomeCardData(System.currentTimeMillis(), "남자사주 애정운 풀이 부탁드립니다.\n" +
                "\n" +
                "그리고 올해운도 궁굼합니다.\n" +
                "\n" +
                "신을무경\n" +
                "사묘인오\n" +
                "\n" +
                "을묘일주 남성입니다.",
                new ArrayList<String>());
        temp.likeCount = 111;
        temp.tags.add("사주");
        temp.tags.add("애정");
        temp.tags.add("당해년도");
        data.add(temp);

        temp = new HomeCardData(System.currentTimeMillis(), "여자: 양력 2015년 5월 26일 시간 진시 입니다\n" +
                "전체적 사주 궁금하고요\n" +
                "신약 사주인지 신강사주 인지와\n" +
                "특히 시주가 식신,시지가 편관인데 편관이 있으면 자식으로인해 힘든사주인지요 말년운도 궁금합니다..감사합니다.",
                new ArrayList<String>());
        temp.replyId  =  "이사벨";
        temp.tags.add("건강");
        temp.replyDate = System.currentTimeMillis();
        temp.likeCount = 12;
        data.add(temp);

        temp = new HomeCardData(System.currentTimeMillis(), "사주에 어떤 살이 있는지 궁금합니다\n" +
                "1992년 6월 14일 양력 밤 12시 25분 입니다\n" +
                "사주말고 살이 궁금합니다 살을 알려주세요.",
                new ArrayList<String>());
        temp.replyId  =  "천도사";
        temp.tags.add("사주");
        temp.tags.add("살풀이");
        temp.replyDate = System.currentTimeMillis();
        temp.likeCount = 42;
        data.add(temp);


        temp = new HomeCardData(System.currentTimeMillis(), "본인사주는 자식에 해당하는 오행도 나약하니 문제고\n" +
                "건강도 안좋아서 빈혈 냉대하 면역성도 약하니 \n" +
                "임신이 안되거나 유산이 가능하고 \n" +
                "아이를 가져도 건강이 약할수 있는사주네요",
                new ArrayList<String>());
        temp.replyId  =  "음향사";
        temp.tags.add("건강");
        temp.tags.add("사주");
        temp.tags.add("살풀이");
        temp.replyDate = System.currentTimeMillis();
        temp.likeCount = 42;
        data.add(temp);

        return data;
    }


    public static Bitmap getTempBitmap(Context context) {
        int name = sBitmapCounter % NUMBER_ASSET_IMAGE;
        sBitmapCounter++;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;
        switch (name) {
            case 0:
                if (mBitmap1 == null || mBitmap1.isRecycled()) {
                    mBitmap1 = BitmapFactory.decodeResource(context.getResources(), R.drawable.image_1, options);
                }
                return mBitmap1;
            case 1:
                if (mBitmap2 == null || mBitmap2.isRecycled()) {
                    mBitmap2 = BitmapFactory.decodeResource(context.getResources(), R.drawable.image_2, options);
                }
                return mBitmap2;
            case 2:
                if (mBitmap3 == null || mBitmap3.isRecycled()) {
                    mBitmap3 = BitmapFactory.decodeResource(context.getResources(), R.drawable.image_3, options);
                }
                return mBitmap3;
            case 3:
                if (mBitmap4 == null || mBitmap4.isRecycled()) {
                    mBitmap4 = BitmapFactory.decodeResource(context.getResources(), R.drawable.image_4, options);
                }
                return mBitmap4;
            case 4:
                if (mBitmap5 == null || mBitmap5.isRecycled()) {
                    mBitmap5 = BitmapFactory.decodeResource(context.getResources(), R.drawable.image_5, options);
                }
                return mBitmap5;
            case 5:
                if (mBitmap6 == null || mBitmap6.isRecycled()) {
                    mBitmap6 = BitmapFactory.decodeResource(context.getResources(), R.drawable.image_6, options);
                }
                return mBitmap6;
            case 6:
                if (mBitmap7 == null || mBitmap7.isRecycled()) {
                    mBitmap7 = BitmapFactory.decodeResource(context.getResources(), R.drawable.image_7, options);
                }
                return mBitmap7;
            case 7:
                if (mBitmap8 == null || mBitmap8.isRecycled()) {
                    mBitmap8 = BitmapFactory.decodeResource(context.getResources(), R.drawable.image_8, options);
                }
                return mBitmap8;
            case 8:
                if (mBitmap9 == null || mBitmap9.isRecycled()) {
                    mBitmap9 = BitmapFactory.decodeResource(context.getResources(), R.drawable.image_9, options);
                }
                return mBitmap9;
            case 9:
                if (mBitmap10 == null || mBitmap10.isRecycled()) {
                    mBitmap10 = BitmapFactory.decodeResource(context.getResources(), R.drawable.image_10, options);
                }
                return mBitmap10;
            case 10:
                if (mBitmap11 == null || mBitmap11.isRecycled()) {
                    mBitmap11 = BitmapFactory.decodeResource(context.getResources(), R.drawable.image_11, options);
                }
                return mBitmap11;
            default:
                return null;
        }
    }

    public static int getTempResource(Context context) {
        int name = sBitmapCounter % NUMBER_ASSET_IMAGE;
        sBitmapCounter++;
        switch (name) {
            case 0:
                return R.drawable.image_1;
            case 1:
                return R.drawable.image_2;
            case 2:
                return R.drawable.image_3;
            case 3:
                return R.drawable.image_4;
            case 4:
                return R.drawable.image_5;
            case 5:
                return R.drawable.image_6;
            case 6:
                return R.drawable.image_7;
            case 7:
                return R.drawable.image_8;
            case 8:
                return R.drawable.image_9;
            case 9:
                return R.drawable.image_10;
            case 10:
                return R.drawable.image_11;
            default:
                return 0;
        }
    }

    public static ArrayList<ShamanData> getDummyShamanData() {
        ArrayList<ShamanData> result = new ArrayList<>();
        result.add(new ShamanData("음향사", 3));
        result.add(new ShamanData("이사벨", 2));
        result.add(new ShamanData("루서", 5));
        result.add(new ShamanData("미카엘", 1));
        result.add(new ShamanData("신당동아저씨", 5));
        result.add(new ShamanData("워커힐동자", 5));
        result.add(new ShamanData("강남할매", 4));
        result.add(new ShamanData("이카루스", 3));
        result.add(new ShamanData("로이", 4));
        result.add(new ShamanData("도담선생", 4));
        return result;
    }
    public static ArrayList<ShamanData> getDummyShamanDataMajor() {
        ArrayList<ShamanData> result = new ArrayList<>();
        result.add(new ShamanData("도담선생", 5));
        result.add(new ShamanData("루시", 5));
        result.add(new ShamanData("선화당", 5));
        result.add(new ShamanData("로이", 5));
        return result;
    }

    public static ArrayList<RealReviewData> getDummyRealReviewData() {
        ArrayList<RealReviewData> result = new ArrayList<>();
        result.add(new RealReviewData("lsooo89", "2020. 1. 10 21:22", "도담선생 > 재물",  "1.3km", 4,
                "" +
                "요즘 이것저것 고민이 너무 많은 시기라 제 자신에 대해 한 번 둘러보고자 신청한 모임이었어요! " +
                "제 자신의 타고난 성격과 성향이 사주로 나오는게 너무 신기했고-앞으로 삶의 방향성에 대해 좀 " +
                "더 확립할 수 있어서 너무 너무 의미있는 시간이었어요!"));
        result.add(new RealReviewData("fatStart22", "2020. 1. 6 15:22", "도담선생 > 애정",  "2.3km", 4,
                "한 마디도 안했는데 그렇게 줄줄 나오는거 보면 신기했다. 그리고 이어지는 질문해봐 라는데 너무 자세히 하나부터 열까지 싹 말해주셔서 크게 질문이 없었다. 결혼 연애 같은걸 억지로 물어봤다. 20~30분 가량 상담이 진행되었고 궁금한건 싹다 알려준다. 말씀을 워낙 잘하시기 때문에 정작 궁금한건 못물어보고 네- 네- 만 하다가 올 수 있으니 질문지를 적어가는 것도 추천한다. "));

        result.add(new RealReviewData("dbwls562", "2020. 1. 5 9:21", "도담청학원 > 직장",  "5.4km", 5,
                "너무너무 추천합니다. 복채예요^^\n" +
                        "\n" +
                        "감사해요~~~\n" +
                        "\n" +
                        "올해도 화이팅입니다!"));

        result.add(new RealReviewData("lognaition87", "2020. 1. 4 22:22", "제시카 > 학업",  "6.3km", 3,
                "나는 이런거에 관심이 없어도 누군가\n" +
                        "관심있어 이야기를 할 때 의미정도는\n" +
                        "알면서 대화를 하는게 좋지 않을까?"));

        result.add(new RealReviewData("rlatkdwls55", "2020. 1. 2 15:22", "도담선생 > 연애",  "7.2km", 4,
                "아직 내 사주도 제대로 풀이하지 못해\n" +
                        "궁합은 보지도 못하는게 현실 ㅋㅋ\n" +
                        "\n" +
                        "내 운명을 바꿔줄 THE 사주가 될지는\n" +
                        "모르겠지만 한 글자씩 더듬어가며\n" +
                        "풀어보다보니 시간이 금방 흐른다 "));

        return result;
    }

    public static ArrayList<HistoryData> getDummyHistoryData() {
        ArrayList<HistoryData> result = new ArrayList<>();
        result.add(new HistoryData("재물", "도담선생", "통화상담", "예약완료", "2020. 1. 10 9:22"));
        result.add(new HistoryData("재물", "이사벨", "영상상담", "예약완료", "2020. 1. 6 21:22"));
        result.add(new HistoryData("직장", "선화당", "통화상담", "예약완료", "2020. 1. 4 11:11"));
        result.add(new HistoryData("연애", "신당동아저씨", "영상상담", "상담완료", "2020. 1. 2 15:22"));
        return result;
    }
}
