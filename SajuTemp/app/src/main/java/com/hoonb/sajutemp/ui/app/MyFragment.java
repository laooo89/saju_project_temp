package com.hoonb.sajutemp.ui.app;

import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.hoonb.sajutemp.R;
import com.hoonb.sajutemp.data.TempDataUtil;

import java.util.ArrayList;

public class MyFragment extends Fragment {


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_my, container, false);
        ListView listView = root.findViewById(R.id.my_listview);
        ArrayList<String> menus = new ArrayList<>();


        menus.add("예약내역");
        menus.add("공지사항");
        menus.add("이벤트");
        menus.add("1:1문의");
        menus.add("환경설정");
        menus.add("앱 정보");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, menus);
        listView.setAdapter(adapter);

        ImageView imageView = root.findViewById(R.id.my_item_thumb);
        imageView.setImageBitmap(TempDataUtil.getTempBitmap(getContext()));
        imageView.setBackground(new ShapeDrawable(new OvalShape()));
        imageView.setClipToOutline(true);
        return root;
    }
}
