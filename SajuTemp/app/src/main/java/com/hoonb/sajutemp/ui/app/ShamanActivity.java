package com.hoonb.sajutemp.ui.app;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.hoonb.sajutemp.R;
import com.hoonb.sajutemp.data.TempDataUtil;

public class ShamanActivity extends FragmentActivity {
    ViewFlipper mViewFlipper;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shaman);

        mViewFlipper = findViewById(R.id.image_slide);
        setFlipImage(this);
        mViewPager = (ViewPager)findViewById(R.id.shaman_view_pager) ;

        mTabLayout = findViewById(R.id.tab_layout);

        // Creating TabPagerAdapter adapter
        TabPagerAdapter pagerAdapter = new TabPagerAdapter(getSupportFragmentManager(), 3);
        mViewPager.setAdapter(pagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));

        // Set TabSelectedListener
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        String id = getIntent().getStringExtra("id");
        TextView tv = findViewById(R.id.shaman_name);
        tv.setText(id);

    }
    public class TabPagerAdapter extends FragmentStatePagerAdapter {

        // Count number of tabs
        private int tabCount;

        public TabPagerAdapter(FragmentManager fm, int tabCount) {
            super(fm);
            this.tabCount = tabCount;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "정보";
                case 1:
                    return "리얼후기";
                case 2:
                    return "위치";
                default:
                    return null;
            }
        }

        @Override
        public Fragment getItem(int position) {

            // Returning the current tabs
            switch (position) {
                case 0:
                    ShamanPageInfoFragment tabFragment1 = new ShamanPageInfoFragment();
                    return tabFragment1;
                case 1:
                    ShamanPageReviewFragment tabFragment2 = new ShamanPageReviewFragment();
                    return tabFragment2;
                case 2:
                    ShamanPageMapFragment tabFragment3 = new ShamanPageMapFragment();
                    return tabFragment3;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabCount;
        }
    }

    private void setFlipImage(Context context) {
        ImageView imageView = new ImageView(context);
        imageView.setImageBitmap(TempDataUtil.getTempBitmap(context));
        imageView.setImageAlpha(180);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        ImageView imageView2 = new ImageView(context);
        imageView2.setImageBitmap(TempDataUtil.getTempBitmap(context));
        imageView2.setImageAlpha(180);
        imageView2.setScaleType(ImageView.ScaleType.CENTER_CROP);

        ImageView imageView3 = new ImageView(context);
        imageView3.setImageBitmap(TempDataUtil.getTempBitmap(context));
        imageView3.setImageAlpha(180);
        imageView3.setScaleType(ImageView.ScaleType.CENTER_CROP);

        mViewFlipper.addView(imageView);      // 이미지 추가
        mViewFlipper.addView(imageView2);      // 이미지 추가
        mViewFlipper.addView(imageView3);      // 이미지 추가
        mViewFlipper.setFlipInterval(4000);       // 자동 이미지 슬라이드 딜레이시간(1000 당 1초)
        mViewFlipper.setAutoStart(true);          // 자동 시작 유무 설정
        // animation
        mViewFlipper.setInAnimation(context, android.R.anim.slide_in_left);
        mViewFlipper.setOutAnimation(context, android.R.anim.slide_out_right);
    }
}
