package com.hoonb.sajutemp.ui.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hoonb.sajutemp.R;
import com.hoonb.sajutemp.data.HomeCardData;
import com.hoonb.sajutemp.data.TempDataUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class HomeFragment extends Fragment {

    private RecyclerView mRecyclerView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        mRecyclerView = root.findViewById(R.id.home_list_view);
        ArrayList<HomeCardData> data = TempDataUtil.getDummyHomeItem();
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(getActivity(), data, R.layout.home_item_layout);
        mRecyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        adapter.notifyDataSetChanged();
        return root;
    }

    public static class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{
        private ArrayList<HomeCardData> homeCardData;
        private int itemLayout;
        private Activity activity;

        public RecyclerViewAdapter(Activity a, ArrayList<HomeCardData> items , int itemLayout){
            this.activity = a;
            this.homeCardData = items;
            this.itemLayout = itemLayout;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(itemLayout, viewGroup,false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, final int position) {
            final HomeCardData item = homeCardData.get(position);
            Log.e("TEST", "bind- " + position);
            final View layout = viewHolder.mView;
            SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");

            // Ask area
            TextView askDateTextView = layout.findViewById(R.id.home_item_ask_date);
            askDateTextView.setText(dayTime.format(new Date(item.date)));
            TextView askDescTextView = layout.findViewById(R.id.home_item_ask_desc);
            askDescTextView.setText(item.content);
            TextView askTagsTextView = layout.findViewById(R.id.home_item_ask_tags);
            StringBuilder tags = new StringBuilder();
            for (int i = 0; i < item.tags.size(); i ++) {
                String tag = item.tags.get(i);
                tags.append("#" + tag);
            }
            askTagsTextView.setText(tags);

            // Reply area
            if (item.replyId == null || item.replyId.isEmpty()) {
                layout.findViewById(R.id.home_item_reply_layout).setVisibility(View.GONE);
            } else {
                layout.findViewById(R.id.home_item_reply_layout).setVisibility(View.VISIBLE);
            }
            TextView shamanId = layout.findViewById(R.id.home_item_shaman_id);
            shamanId.setText(item.replyId);
            ImageView thumb = layout.findViewById(R.id.home_item_thumb);
            thumb.setImageBitmap(TempDataUtil.getTempBitmap(layout.getContext()));
            thumb.setBackground(new ShapeDrawable(new OvalShape()));
            thumb.setClipToOutline(true);
            TextView replyDate = layout.findViewById(R.id.home_item_reply_date);
            replyDate.setText(dayTime.format(new Date(item.replyDate)));

            // Like bar
            TextView likeCount = layout.findViewById(R.id.home_item_like_cnt);
            likeCount.setText(String.valueOf(item.likeCount));
            TextView cntCount = layout.findViewById(R.id.home_item_comment_cnt);
            cntCount.setText(String.valueOf(item.commentList.size()));


            //Here it is simply write onItemClick listener here
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(activity,  ShamanActivity.class);
                    i.putExtra("id", item.replyId);
                    activity.startActivity(i);
                }
            });
        }

        @Override
        public int getItemCount() {
            return homeCardData.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public ViewHolder(View itemView) {
                super(itemView);
                mView = itemView;
            }
        }
    }
}
