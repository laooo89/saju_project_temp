package com.hoonb.sajutemp.data;

public class ShamanData {
    public String shamanId;
    public int stars;

    public ShamanData(String shamanId, int stars) {
        this.shamanId = shamanId;
        this.stars = stars;
    }
}
