package com.hoonb.sajutemp.data;

import java.util.ArrayList;
import java.util.HashMap;

public class HomeCardData {
    public long date;
    public String content;
    public ArrayList<String> tags;
    public String replyId;
    public long replyDate;
    public String replyAudio;
    public int likeCount = 0;
    public ArrayList<SimpleComment> commentList;

    public HomeCardData(long date, String content, ArrayList<String> tags) {
        this.date = date;
        this.content = content;
        this.tags = tags;
        this.commentList = new ArrayList<>();
    }
}
