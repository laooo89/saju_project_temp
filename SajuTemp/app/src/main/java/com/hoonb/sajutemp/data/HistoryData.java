package com.hoonb.sajutemp.data;

public class HistoryData {

    public String category;
    public String shamanId;
    public String type;
    public String status;
    public String date;

    public HistoryData(String category, String shamanId, String type, String status, String date) {
        this.category = category;
        this.shamanId = shamanId;
        this.type = type;
        this.status = status;
        this.date = date;
    }
}
