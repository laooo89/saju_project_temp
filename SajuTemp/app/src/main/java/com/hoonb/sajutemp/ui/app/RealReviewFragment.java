package com.hoonb.sajutemp.ui.app;

import android.content.Context;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.hoonb.sajutemp.R;
import com.hoonb.sajutemp.data.RealReviewData;
import com.hoonb.sajutemp.data.ShamanData;
import com.hoonb.sajutemp.data.TempDataUtil;

import java.util.ArrayList;

public class RealReviewFragment extends Fragment {

    ViewFlipper mViewFlipper;
    private ListView mRealReviewListView;
    private ArrayList<RealReviewData> mRealReviewData;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_realreview, container, false);
        mViewFlipper = root.findViewById(R.id.image_slide);
        setFlipImage(getContext());
        setHasOptionsMenu(true);
        mRealReviewListView = root.findViewById(R.id.realreview_list);
        mRealReviewData = TempDataUtil.getDummyRealReviewData();

        ListAdapter adapter1 = new ListAdapter(mRealReviewData);
        mRealReviewListView.setAdapter(adapter1);

        return root;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.shaman_menu, menu);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().invalidateOptionsMenu();
    }

    private void setFlipImage(Context context) {
        ImageView imageView = new ImageView(context);
        imageView.setImageBitmap(TempDataUtil.getTempBitmap(context));
        imageView.setImageAlpha(180);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        ImageView imageView2 = new ImageView(context);
        imageView2.setImageBitmap(TempDataUtil.getTempBitmap(context));
        imageView2.setImageAlpha(180);
        imageView2.setScaleType(ImageView.ScaleType.CENTER_CROP);

        ImageView imageView3 = new ImageView(context);
        imageView3.setImageBitmap(TempDataUtil.getTempBitmap(context));
        imageView3.setImageAlpha(180);
        imageView3.setScaleType(ImageView.ScaleType.CENTER_CROP);

        mViewFlipper.addView(imageView);      // 이미지 추가
        mViewFlipper.addView(imageView2);      // 이미지 추가
        mViewFlipper.addView(imageView3);      // 이미지 추가
        mViewFlipper.setFlipInterval(4000);       // 자동 이미지 슬라이드 딜레이시간(1000 당 1초)
        mViewFlipper.setAutoStart(true);          // 자동 시작 유무 설정
        // animation
        mViewFlipper.setInAnimation(context, android.R.anim.slide_in_left);
        mViewFlipper.setOutAnimation(context, android.R.anim.slide_out_right);
    }

    public class ListAdapter extends BaseAdapter {
        LayoutInflater inflater = null;
        private ArrayList<RealReviewData> mData = null;

        public ListAdapter(ArrayList<RealReviewData> data) {
            mData = data;
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                final Context context = parent.getContext();
                if (inflater == null) {
                    inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                }
                convertView = inflater.inflate(R.layout.realreview_item_layout, parent, false);
            }

            TextView idTextView = (TextView) convertView.findViewById(R.id.review_item_id);
            idTextView.setText(mData.get(position).id);
            TextView category = (TextView) convertView.findViewById(R.id.review_category);
            category.setText(String.valueOf(mData.get(position).category));
            TextView distance = (TextView) convertView.findViewById(R.id.review_distance);
            distance.setText(String.valueOf(mData.get(position).distance));
            TextView date = (TextView) convertView.findViewById(R.id.review_item_date);
            date.setText(String.valueOf(mData.get(position).date));
            TextView review = (TextView) convertView.findViewById(R.id.review_realreview);
            review.setText(String.valueOf(mData.get(position).review));
            RatingBar ratingBar = convertView.findViewById(R.id.ratingBar);
            ratingBar.setRating(mData.get(position).stars);

            ImageView imageView = convertView.findViewById(R.id.review_item_thumb);
            imageView.setImageBitmap(TempDataUtil.getTempBitmap(convertView.getContext()));
            imageView.setBackground(new ShapeDrawable(new OvalShape()));
            imageView.setClipToOutline(true);
            return convertView;
        }
    }
}
