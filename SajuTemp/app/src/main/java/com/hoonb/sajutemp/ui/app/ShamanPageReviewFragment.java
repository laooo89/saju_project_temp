package com.hoonb.sajutemp.ui.app;

import android.content.Context;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.hoonb.sajutemp.R;
import com.hoonb.sajutemp.data.RealReviewData;
import com.hoonb.sajutemp.data.TempDataUtil;

import java.util.ArrayList;

public class ShamanPageReviewFragment extends Fragment {
    private ListView mRealReviewListView;
    private ArrayList<RealReviewData> mRealReviewData;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.page_shaman_review, container, false);
        mRealReviewListView = root.findViewById(R.id.shaman_review_list);
        mRealReviewData = TempDataUtil.getDummyRealReviewData();

        ListAdapter adapter1 = new ListAdapter(mRealReviewData);
        mRealReviewListView.setAdapter(adapter1);
        return root;
    }

    public class ListAdapter extends BaseAdapter {
        LayoutInflater inflater = null;
        private ArrayList<RealReviewData> mData = null;

        public ListAdapter(ArrayList<RealReviewData> data) {
            mData = data;
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                final Context context = parent.getContext();
                if (inflater == null) {
                    inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                }
                convertView = inflater.inflate(R.layout.realreview_item_layout, parent, false);
            }

            TextView idTextView = (TextView) convertView.findViewById(R.id.review_item_id);
            idTextView.setText(mData.get(position).id);
            TextView category = (TextView) convertView.findViewById(R.id.review_category);
            category.setText(String.valueOf(mData.get(position).category));
            TextView distance = (TextView) convertView.findViewById(R.id.review_distance);
            distance.setText(String.valueOf(mData.get(position).distance));
            TextView date = (TextView) convertView.findViewById(R.id.review_item_date);
            date.setText(String.valueOf(mData.get(position).date));
            TextView review = (TextView) convertView.findViewById(R.id.review_realreview);
            review.setText(String.valueOf(mData.get(position).review));
            RatingBar ratingBar = convertView.findViewById(R.id.ratingBar);
            ratingBar.setRating(mData.get(position).stars);

            ImageView imageView = convertView.findViewById(R.id.review_item_thumb);
            imageView.setImageBitmap(TempDataUtil.getTempBitmap(convertView.getContext()));
            imageView.setBackground(new ShapeDrawable(new OvalShape()));
            imageView.setClipToOutline(true);
            return convertView;
        }
    }
}
