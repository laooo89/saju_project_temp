package com.hoonb.sajutemp.ui.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hoonb.sajutemp.R;
import com.hoonb.sajutemp.data.HistoryData;
import com.hoonb.sajutemp.data.TempDataUtil;

import java.util.ArrayList;

public class HistoryFragment extends Fragment {

    private RecyclerView mRecyclerView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_history, container, false);
        mRecyclerView = root.findViewById(R.id.history_list_view);
        ArrayList<HistoryData> data = TempDataUtil.getDummyHistoryData();
        HistoryFragment.RecyclerViewAdapter adapter = new HistoryFragment.RecyclerViewAdapter(getActivity(), data, R.layout.history_item_layout);
        mRecyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        adapter.notifyDataSetChanged();
        return root;
    }

    public static class RecyclerViewAdapter extends RecyclerView.Adapter<HistoryFragment.RecyclerViewAdapter.ViewHolder>{
        private ArrayList<HistoryData> historyCardData;
        private int itemLayout;
        private Activity activity;

        public RecyclerViewAdapter(Activity activity, ArrayList<HistoryData> items , int itemLayout){
            this.activity = activity;
            this.historyCardData = items;
            this.itemLayout = itemLayout;
        }

        @Override
        public HistoryFragment.RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(itemLayout, viewGroup,false);
            return new HistoryFragment.RecyclerViewAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(HistoryFragment.RecyclerViewAdapter.ViewHolder viewHolder, final int position) {
            final HistoryData item = historyCardData.get(position);
            View layout = viewHolder.mView;

            TextView category = layout.findViewById(R.id.history_category);
            category.setText(item.category);
            TextView status = layout.findViewById(R.id.history_status);
            status.setText(item.status);
            TextView shamanId = layout.findViewById(R.id.history_shaman_id);
            shamanId.setText(item.shamanId);
            TextView type = layout.findViewById(R.id.history_type);
            type.setText(item.type);
            TextView date = layout.findViewById(R.id.history_date);
            date.setText(item.date);


            if (item.status.equalsIgnoreCase("상담완료"))  {
                status.setTextColor(Color.BLUE);
            }
            //Here it is simply write onItemClick listener here
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    Intent i = new Intent(context,  ShamanActivity.class);
                    i.putExtra("id", item.shamanId);
                    activity.startActivity(i);
                }
            });
        }

        @Override
        public int getItemCount() {
            return historyCardData.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public ViewHolder(View itemView) {
                super(itemView);
                mView = itemView;
            }
        }
    }
}
