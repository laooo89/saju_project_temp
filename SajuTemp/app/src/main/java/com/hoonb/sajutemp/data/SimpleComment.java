package com.hoonb.sajutemp.data;

public class SimpleComment {
    public String id;
    public String comment;

    public SimpleComment(String id, String comment) {
        this.id = id;
        this.comment = comment;
    }
}
