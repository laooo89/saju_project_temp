package com.hoonb.sajutemp.data;

public class RealReviewData {
    public String id;
    public String date;
    public String category;
    public String distance;
    public int stars;
    public String review;

    public RealReviewData(String id, String date, String category, String distance, int stars, String review) {
        this.id = id;
        this.date = date;
        this.category = category;
        this.distance = distance;
        this.stars = stars;
        this.review = review;
    }
}
