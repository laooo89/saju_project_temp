package com.hoonb.sajutemp.ui.app;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.hoonb.sajutemp.R;

public class ShamanPageInfoFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.page_shaman_info, container, false);
        TextView tv = root.findViewById(R.id.info);
        tv.setText("누구에든 털어 놓을 수 없는 고민 저에게 노크 해주세요^^\n" +
                "행복은 어쩌다 한번 있는 커다란 행운이 아니예요.\n" +
                "우리가 살아가는 일상생활에서 매일 발생하는 작은 친절이나 기쁨속에 있습니다.\n" +
                "당신의 텅빈 마음, 공허한 마음, 헛헛한 마음..\n" +
                "기대고 싶은 마음, 털어놓고싶은 그 많은 말들, 끝나지 않을 당신의 미련.\n" +
                "미련이 미련하게 많은 그대가 내일은 그 미련에 감사할 날도 올 수 있답니다.");
        return root;
    }
}
